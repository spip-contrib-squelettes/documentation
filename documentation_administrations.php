<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/cextras');
include_spip('base/documentation');
	
function documentation_upgrade($nom_meta_base_version,$version_cible){
	$maj 	= array();
	$champs = documentation_declarer_champs_extras();
	include_spip('base/upgrade');
	cextras_api_upgrade($champs,$maj['1.2.0']);
	maj_plugin($nom_meta_base_version, $version_cible,$maj);
}

function documentation_vider_tables($nom_meta_base_version) {
	$champs = documentation_declarer_champs_extras();
	cextras_api_vider_tables($champs);
	effacer_meta($nom_meta_base_version);
}
?>
