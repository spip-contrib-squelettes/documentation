<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

/*
 * Codes pour avoir des forums ouverts
 * sur les tickets...
 * Mais les spammeurs sont trop corriaces !
 * Ce code n'est plus actif
 *
 */


function doc_nospam_tester_spam($texte, $params) {
	$infos = analyser_spams($texte);
	if ($infos['nombre_liens'] > 0) {
		foreach ($params as $desc) {
			list($nom, $op, $val) = $desc;
			eval("\$test = (\$infos['$nom'] $op $val);");
			if ($test) {
				return true;
			}
		}
	}
	return false;
}


// verifier qu'un ticket identique n'a pas ete publie il y a peu
function test_doublon_ticket($champs, $table='spip_tickets'){
	$champs[] = "maj>DATE_SUB(NOW(),INTERVAL 1 hour)";
	return sql_countsel($table, $champs);
}

// verifier que cette ip n'en est pas a son N-ieme post en peu de temps
// plus de 5 messages en 5 minutes c'est suspect ...
function test_ticket_ip_furieuse(){
	$champs = array();
	$champs[] = 'ip=' . sql_quote($GLOBALS['ip']);
	$champs[] =  "maj>DATE_SUB(NOW(),INTERVAL 10 minute)";
	$nb1 = sql_countsel('spip_tickets',$champs);
	$nb2 = sql_countsel('spip_tickets_forum',$champs);
	if (($nb1 + $nb2) > 5) {
		return true;
	}	
	return false;
}



			
// ne pas accepter ce qu'on peut appeler du spam
function documentation_formulaire_verifier($flux){
	if ($flux['args']['form'] == 'editer_ticket'
	OR  $flux['args']['form'] == 'forum_ticket') {
		if (include_spip('inc/nospam')) {
			$is_commentaire = ($flux['args']['form'] == 'forum_ticket');
			
			$testsA = $testsB = $testsC = $testsM = array(
				array('caracteres_texte_lien_min', '<', 4),
				array('nombre_liens', '>', 3),
				array('caracteres_utiles', '<', 10),
			);
			$testsB[1][2] = 0; // nombre_liens > 0
			$testsC[1][2] = 1; // nombre_liens > 1
			$testsC[2][2] = 3; // caracteres_utiles < 3
			$testsM[2][2] = 5; // caracteres_utiles < 5

			if (!$is_commentaire) { // ticket
				if (doc_nospam_tester_spam(_request('texte'),   $testsA)
				or  doc_nospam_tester_spam(_request('titre'),   $testsB)
				or  doc_nospam_tester_spam(_request('exemple'), $testsC)) {
					$flux['data']['message_erreur'] .= _T('nospam:erreur_spam');
				}
			} else { // commentaire
				if (doc_nospam_tester_spam(_request('texte'),   $testsM)) {
					$flux['data']['message_erreur'] .= _T('nospam:erreur_spam');
				}
			}
			// doublons
			$table = $is_commentaire ? 'spip_tickets_forum' : 'spip_tickets';
			if (test_doublon_ticket(array('texte='. sql_quote(_request('texte'))), $table)) {
				$flux['data']['message_erreur'] .= _T('nospam:erreur_spam_doublon');
			}
			// ip vilaine
			if (test_ticket_ip_furieuse()) {
				$flux['data']['message_erreur'] .= _T('nospam:erreur_spam_ip');
			}			
		}
	}
	return $flux;
}


// autorisations correspondantes :
function documentation_autoriser(){}

// Autorisation de commenter des tickets
function autoriser_ticket_commenter($faire, $type, $id, $qui, $opt){
	// toujours
	return true;
}

// Autorisation de creer ou modifier des tickets
function autoriser_ticket_creer($faire, $type, $id, $qui, $opt){
	return autoriser("ecrire", "ticket", $id, $qui, $opt);
}

// Autorisation de creer ou modifier des tickets
function autoriser_ticket_ecrire($faire, $type, $id, $qui, $opt){
	// creer : toujours
	if (!intval($id)) return true;
	// modifier : admin ou le redacteur de ticket.
	if (!$qui['id_auteur']) return false;
	if ($qui['statut'] == '0minirezo' AND !$qui['restreint']) return true;
	return sql_allfetsel("id_auteur", "spip_tickets", "id_auteur=" . $qui['id_auteur']);
}


?>
