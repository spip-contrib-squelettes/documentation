<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/documentation?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteur' => 'Autor:',

	// C
	'cfg_descriptif_documentation' => 'Opciones del esqueleto de documentación',
	'cfg_titre_documentation' => 'Esqueleto "Documentación"',
	'champ_auteur' => 'Autor(es)',
	'champ_date' => 'Fecha',
	'champ_id' => 'Id',
	'champ_langue' => 'Idioma',
	'champ_resume' => 'Resumen',
	'champ_texte' => 'Texto',
	'champ_titre' => 'Título',
	'chapitre' => 'Capítulo: ',
	'choisir' => 'Elegir...',
	'commentaire' => '@nb@ comentario',
	'commentaire_aucun' => 'Ningún comentario',
	'commentaires' => '@nb@ comentarios',
	'commentez' => 'Comentar la documentación',
	'conception_graphique' => 'Concepción gráfica',
	'conception_graphique_adaptee_par' => 'por',
	'conception_graphique_de' => 'Tema gráfico de ',
	'conception_graphique_par' => 'Tema gráfico adaptado de ',
	'creer_nouvelle_suggestion' => 'Proponer una nueva sugerencia',

	// D
	'description' => 'Descripción',
	'documentation_papier' => '¡Documentación papel!',
	'documentation_papier_complement' => 'Para leer tranquilamente...',

	// E
	'editer_suggestion' => 'Editar esta sugerencia',
	'en_savoir_plus' => '¡Saber más!',
	'erreur_de_chargement_ajax' => '¡Error al cargar AJAX!',
	'erreur_inscription_desactivee' => 'Los registros están desactivados para este sitio.',
	'erreur_inscription_session' => 'Ya está identificado.',
	'exemple' => 'Ejemplo',
	'exercice' => 'Ejercicio',
	'explication_activer_formulaire_ecrire_auteur' => '¿Mostrar el formulario para escribir a un autor o a los visitantes no identificados?',
	'explication_barre_menu_absente' => 'El plugin Menú permite mostrar una barra de pestañas y definir sus entradas. 
¡Activar la opción para no mostrarlo!',
	'explication_css_geshi' => '¿Utilizar una CSS para Geshi (Coloración de código) única (y no una definición justa encima de los códigos)?',
	'explication_description_suggestion' => 'Indique una errata, proponga una reformulación, susurre al oído un bug, en fin, ¡explíquese en el sitio y su contenido!
	',
	'explication_interdire_recherche_tickets' => 'Si marca esta opción, los tickets no serán mostrados en el motor de búsqueda del sitio.',
	'explication_navigation_ajax' => 'Mostrar todos los artículos y sección en la columna de navegación puede ser costoso en la actuación y voluminoso en términos de bytes a enviar si la documentación contiene numerosas páginas.
Esta opción permite limitar el árbol de directorios enviado al sector en curso de lectura, el resto puede obtenerse en AJAX a la vista de los otros sectores.',
	'explication_taille_redimensionnement_image' => 'Dependiendo del tema elegido, el tamaño de escala de imagen puede ser demasiado pequeño o demasiado grande (por defecto 440px de ancho) con respecto a la anchura de la columna de contenido. Elija un valor lo más adaptado a su tema gráfico.',
	'explication_utiliser_champs_extras' => 'Marque los campos añadidos por el esqueleto que no desea utilizar.',

	// I
	'icones_par' => 'Iconos adaptados del tema',
	'integrale' => '¡Íntegro!',

	// L
	'label_activer_formulaire_ecrire_auteur' => 'Escribir a un autor',
	'label_avancement' => 'Porcentaje completado',
	'label_barre_menu_absente' => 'Quitar la barra de menú',
	'label_charger_url' => 'Acceso rápido:',
	'label_css_geshi' => 'CSS de Geshi',
	'label_exemple' => 'Ejemplo',
	'label_exercice' => 'Ejercicio',
	'label_interdire_recherche_tickets' => 'No mostrar los tickets en la búsqueda',
	'label_navigation_ajax' => 'Navegación AJAX',
	'label_reponse' => 'Respuesta',
	'label_secteur_langue' => '¿Utilizar un sector por idioma?',
	'label_sepia_logo' => '¡Color sepia!',
	'label_sepia_logo_nb' => '¡Color sepia n&b!',
	'label_sous_titre_sommaire' => 'Subtítulo de la página sumario',
	'label_taille_redimensionnement_image' => 'Anchura máxima de las imágenes',
	'label_titre_sommaire' => 'Título de la página sumario',
	'label_utiliser_champs_extras' => 'No utilizar los campos suplementarios',
	'label_version' => 'Versión de la documentación',
	'licence' => 'Licencia:',
	'lien_sedna' => 'Sitios que seguimos',
	'lien_sedna_img' => 'Sedna',

	// M
	'maj' => 'Revisión del ',
	'mentions_legales' => 'Menciones legales',
	'mis_a_jour' => 'Actualizado:',
	'mots_cles' => 'Palabras clave',

	// N
	'navigation_clavier' => '¡Puede pasar las páginas con las flechas de izquierda y derecha de su teclado!',
	'nom' => 'Nombre',
	'nouvelle_suggestion' => 'Nueva sugerencia',

	// P
	'partez_a_laventure' => '¡Aventúrese!',
	'precedent' => 'Precedente',
	'proposer_suggestion' => '¡Proponga una mejora!',
	'proposer_suggestion_img' => 'Administrador de tickets',
	'publie_le' => 'Publicado el:',

	// R
	'reponse' => 'Respuesta',

	// S
	'signaler_coquille' => 'Indique una errata...',
	'sinscrire' => 'Registrarse',
	'sommaire' => 'Contenido',
	'sommaire_livre' => 'Sumario',
	'sous_licence' => 'bajo licencia',
	'suggestion' => 'Sugerencia',
	'suggestions' => 'Sugerencias',
	'suivant' => 'Siguiente',
	'suivi' => 'Seguimiento',
	'suivi_dernieres_modifications_articles' => 'Últimos cambios en los artículos',
	'suivi_derniers_articles' => 'Últimos artículos',
	'suivi_derniers_articles_proposes' => 'Últimos artículos propuestos',
	'suivi_derniers_commentaires' => 'Últimos comentarios',
	'suivi_description' => 'Seguimiento del sitio...',
	'symboles' => 'Símbolos',

	// T
	'table_des_matieres' => 'Contenido',
	'tickets_sur_inscription' => 'La redacción de tickets o comentarios no es posible más que para las personas identificadas.',
	'titre_articles_lies' => 'Artículos complementarios',
	'titre_identification' => 'Identificación',
	'titre_inscription' => 'Registro',
	'tout_voir' => 'Ver todo',
	'traductions' => 'Traducciones:'
);
