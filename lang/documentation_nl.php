<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/documentation?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteur' => 'Auteur:',

	// C
	'cfg_descriptif_documentation' => 'Opties van het documentatie-skelet',
	'cfg_titre_documentation' => 'Skelet "Documentation"',
	'champ_auteur' => 'Auteur(s)',
	'champ_date' => 'Datum',
	'champ_id' => 'Id',
	'champ_langue' => 'Taal',
	'champ_resume' => 'Samenvatting',
	'champ_texte' => 'Tekst',
	'champ_titre' => 'Titel',
	'chapitre' => 'Hoofdstuk: ',
	'choisir' => 'Kies...',
	'commentaire' => '@nb@ commentaar',
	'commentaire_aucun' => 'Geen commentaar',
	'commentaires' => '@nb@ commentaren',
	'commentez' => 'Becommentarieer de documentatie',
	'conception_graphique' => 'Grafisch concept',
	'conception_graphique_adaptee_par' => 'door',
	'conception_graphique_de' => 'Grafisch thema door ',
	'conception_graphique_par' => 'Grafisch thema aangepast van ',
	'creer_nouvelle_suggestion' => 'Doe een nieuwe suggestie',

	// D
	'description' => 'Omschrijving',
	'documentation_papier' => 'Papieren documentatie!',
	'documentation_papier_complement' => 'Om rustig na te lezen...',

	// E
	'editer_suggestion' => 'Pas deze suggestie aan',
	'en_savoir_plus' => 'Meer weten!',
	'erreur_de_chargement_ajax' => 'Fout bij laden AJAX!',
	'erreur_inscription_desactivee' => 'Inschrijving zijn op deze site niet actief.',
	'erreur_inscription_session' => 'Je bent al aangemeld.',
	'exemple' => 'Voorbeeld',
	'exercice' => 'Oefening',
	'explication_activer_formulaire_ecrire_auteur' => 'Het formulier tonen om niet-aangemelde bezoekers de auteur te laten schrijven?',
	'explication_barre_menu_absente' => 'De plugin Menu laat toe
		een balk met tabbladen te definiëren en weer te geven.
		Activeer de optie om hem niet te tonen!',
	'explication_css_geshi' => 'Een CSS gebruiken om Geshi
		(Kleurcodering) te gebruiken
		(en niet een definitie net boven de codes)?',
	'explication_description_suggestion' => '
		Meld een fout,
		stel een herformulering voor,
		rapporteer een bug,
		laat wat weten over de site en zijn inhoud!
	',
	'explication_interdire_recherche_tickets' => 'Wanneer deze optie is aangekruist, worden tickets niet door de zoekmachine van de site weergegeven.',
	'explication_navigation_ajax' => 'Alle artikelen en rubrieken
		in de navigatiekolom tonen kan een kostbare zaak zijn
		voor de performance en hoeveelheid data die moet worden verzonden
		als de documentatie uit veel bladzijdes bestaat. Deze
		optie beperkt de hoeveelheid gegevens tot die van de hoofdrubriek,
		terwijl de rest via AJAX kan worden opgevraagd door
		de muis over een andere hoofdrubriek te bewegen.',
	'explication_taille_redimensionnement_image' => 'Afhankelijk van het gekozen thema, kunnen
		afbeeldingen te klein of te groot worden weergegeven
		(standaard 440px breed) ten opzichte van de breedte van
		de kolom met inhoud. Kies een beter op het grafische thema aangepaste waarde.',
	'explication_utiliser_champs_extras' => 'Klik de door het skelet toegevoegde velden aan die je niet wilt gebruiken.',

	// I
	'icones_par' => 'Aan het thema aangepaste iconen ',
	'integrale' => 'Het geheel!',

	// L
	'label_activer_formulaire_ecrire_auteur' => 'Schrijf naar een auteur',
	'label_avancement' => 'Gerealiseerd percentage',
	'label_barre_menu_absente' => 'Verwijder de menubalk',
	'label_charger_url' => 'Snelle toegang:',
	'label_css_geshi' => 'CSS van Geshi',
	'label_exemple' => 'Voorbeeld',
	'label_exercice' => 'Oefening',
	'label_interdire_recherche_tickets' => 'Geen tickets in de zoekresultaten weergeven',
	'label_navigation_ajax' => 'AJAX navigatie',
	'label_reponse' => 'Antwoord',
	'label_secteur_langue' => 'Per taal een hoofdrubriek gebruiken?',
	'label_sepia_logo' => 'Kleur van sepia!',
	'label_sepia_logo_nb' => 'Kleur van sepia zwart/wit!',
	'label_sous_titre_sommaire' => 'Subtitel van de beginpagina',
	'label_taille_redimensionnement_image' => 'Maximale breedte van afbeeldingen',
	'label_titre_sommaire' => 'Titel van de beginpagina',
	'label_utiliser_champs_extras' => 'Geen aanvullende velden gebruiken',
	'label_version' => 'Versie van de documentatie',
	'licence' => 'Licentie:',
	'lien_sedna' => 'Sites die we volgen',
	'lien_sedna_img' => 'Sedna',

	// M
	'maj' => 'Herziening van ',
	'mentions_legales' => 'Wettelijke bepalingen',
	'mis_a_jour' => 'Aanpassing:',
	'mots_cles' => 'Trefwoorden',

	// N
	'navigation_clavier' => 'Je kunt de bladzijdes omslaan met
			de linker en rechter pijl op het toetsenbord!',
	'nom' => 'Naam',
	'nouvelle_suggestion' => 'Nieuwe suggestie',

	// P
	'partez_a_laventure' => 'Doe mee in het avontuur!',
	'precedent' => 'Vorige',
	'proposer_suggestion' => 'Stel een verbetering voor!',
	'proposer_suggestion_img' => 'Beheer van tickets',
	'publie_le' => 'Gepubliceerd op:',

	// R
	'reponse' => 'Antwoord',

	// S
	'signaler_coquille' => 'Een fout melden…',
	'sinscrire' => 'Zich inschrijven',
	'sommaire' => 'Inhoud',
	'sommaire_livre' => 'Opsomming',
	'sous_licence' => 'onder licentie',
	'suggestion' => 'Suggestie',
	'suggestions' => 'Suggesties',
	'suivant' => 'Volgende',
	'suivi' => 'Gevolgd',
	'suivi_dernieres_modifications_articles' => 'Laatste aanpassingen van artikelen',
	'suivi_derniers_articles' => 'Laatste artikelen',
	'suivi_derniers_articles_proposes' => 'Nieuwste voorgestelde artikelen',
	'suivi_derniers_commentaires' => 'Laatste commentaren',
	'suivi_description' => 'Site opvolging...',
	'symboles' => 'Symbolen',

	// T
	'table_des_matieres' => 'Inhoudsopgave',
	'tickets_sur_inscription' => '
		Het maken van tickets en commentaren is voorbehouden aan
		aangemelde personen.
	',
	'titre_articles_lies' => 'Aanvullende artikelen',
	'titre_identification' => 'Identificatie',
	'titre_inscription' => 'Inschrijving',
	'tout_voir' => 'Alles bekijken',
	'traductions' => 'Vertalingen:'
);
